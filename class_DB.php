<?php 
date_default_timezone_set('Asia/Taipei');
class DB{	
		
		//建構子
		function __construct(){			//echo "Tools is constructed\n";
			
		}

		//連結資料庫(ip,使用者,密碼)
		public function DB_connMysql($mdb_username,$mdb_password){
			$db_link = mysql_connect("127.0.0.1",$mdb_username,$mdb_password);
			if(!$db_link){
				die("ERROR IP");

			}else{				
				mysql_query("SET NAMES 'utf8'");
			}			
		}

		//選擇DB(DB名稱)
		public function DB_select_db($mdb_name){
			$db_select=mysql_select_db($mdb_name);
			if(!$db_select) {
				die("ERROR DB");
			}
		}
		
		//找FB_token
		public function DB_find_FBToken($mUserFBToken){
			$sql_query = "SELECT user_FB_token FROM user WHERE user_FB_token='$mUserFBToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				return mysql_num_rows($result);
			}			
								
		}

		//找session_token
		public function DB_find_session_token($mUserSessionToken){
			$sql_query = "SELECT user_session_token FROM user WHERE user_session_token='$mUserSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				return mysql_num_rows($result);
			}			
								
		}		

		//更新session_token
		public function DB_update_FBToken($mUserFBToken,$mSessionToken){
			$sessionTokenUpdate= "UPDATE user SET user_session_token='$mSessionToken' WHERE user_FB_token='$mUserFBToken'";
			$result=mysql_query($sessionTokenUpdate);
			if(!$result){
				die("ERROR");
			}						
		}

		//建立使用者
		public function DB_user_sign($mUserFBToken,$mUserName,$nUserSex,$mUserAge,$mUserWeight,$mUserHeight,$mUserWorkIntensity,$mUserSessionToken,$mUserCal){
			$mToday = date("dmY", strtotime('today'));
			$sql_query = "INSERT INTO user(user_fb_token,user_name,user_sex,user_age,user_weight,user_height,user_work_intensity,user_session_token,user_cal,user_sign_date)VALUE(
				'$mUserFBToken','$mUserName','$nUserSex','$mUserAge','$mUserWeight','$mUserHeight','$mUserWorkIntensity','$mUserSessionToken','$mUserCal','$mToday')";
			$result=mysql_query($sql_query);	
			if(!$result){
				die("ERROR");
			}else{
				$cart = array(
				"msg"=>"Success",
				"session_token"=>$mUserSessionToken
				);
			$output = json_encode($cart);
			echo ($output."\n");
	
			}						
		}			

		//清空sessiontoken(FBtoken)
		public function DB_session_token_claer($mFBToken){
			$sessionTokenDelete= "UPDATE user SET user_session_token = null WHERE user_fb_token='$mFBToken'";
			$result=mysql_query($sessionTokenDelete);	
			if(!$result){
				die("ERROR");
			}			
		}

		//logout
		public function DB_logout($mSessionToken){
			$sessionTokenDelete= "UPDATE user SET user_session_token = null WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sessionTokenDelete);	
			if(!$result){
				die("ERROR");
			}else{
				$cart = array(
				"msg"=>"Logout Success",				
				);
			$output = json_encode($cart);
			echo($output);
			}					
		}

		//user_session_token尋找ID
		public function DB_find_user_id($mSessionToken)
		{
			$sql_query = "SELECT user_id FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"Session Token overtime",				
					);
					$output = json_encode($cart);
					echo($output);
					exit();
				}else{
					$DBData=mysql_fetch_row($result);
					return $DBData[0];	
				}			
			}			
		}

		//user_session_token尋找sex
		public function DB_find_user_sex($mSessionToken)
		{
			$sql_query = "SELECT user_sex FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"Session Token overtime",				
					);
					$output = json_encode($cart);
					echo($output);
					exit();
				}else{
					$DBData=mysql_fetch_row($result);
					return $DBData[0];	
				}			
			}			
		}

		//user_session_token尋找工作強度
		public function DB_find_user_work_intensity($mSessionToken)
		{
			$sql_query = "SELECT user_work_intensity FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"Session Token overtime",				
					);
					$output = json_encode($cart);
					echo($output);
					exit();
				}else{
					$DBData=mysql_fetch_row($result);
					return $DBData[0];	
				}			
			}			
		}

		//user_session_token尋找age
		public function DB_find_user_age($mSessionToken)
		{
			$sql_query = "SELECT user_age FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"Session Token overtime",				
					);
					$output = json_encode($cart);
					echo($output);
					exit();
				}else{
					$DBData=mysql_fetch_row($result);
					return $DBData[0];	
				}			
			}			
		}

		//user_session_token尋找cal
		public function DB_find_user_cal($mSessionToken)
		{
			$sql_query = "SELECT user_cal FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"Session Token overtime",				
					);
					$output = json_encode($cart);
					echo($output);
					exit();
				}else{
					$DBData=mysql_fetch_row($result);
					return $DBData[0];	
				}			
			}			
		}

		//user_session_token尋找user_task_point
		public function DB_find_user_task_point($mSessionToken)
		{
			$sql_query = "SELECT user_task_point FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"Session Token overtime",				
					);
					$output = json_encode($cart);
					echo($output);
					exit();
				}else{
					$DBData=mysql_fetch_row($result);
					$cart = array(
					"task_point"=>$DBData[0],				
					);
					$output = json_encode($cart);
					echo($output);	
				}			
			}			
		}

		//刪除使用者
		public function DB_user_delete($mSessionToken)
		{			
			$sql_query = "DELETE FROM user WHERE user_session_token='$mSessionToken'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				$cart = array(
					"msg"=>"User Reset!",				
					);
					$output = json_encode($cart);
					echo($output);
			}		
		}

		//給予體重
		public function DB_for_show_weight($mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			//找體重
			$sql_query = "SELECT weight_day,waistline,body_fat,weight_day_date,height FROM weight_day WHERE user_id='$mUserID'";
			$result=mysql_query($sql_query);
			//找CAL
			$sql_query_cal = "SELECT user_cal,user_weight,user_height FROM user WHERE user_session_token='$mSessionToken'";
			$result_cal=mysql_query($sql_query_cal);

			if(!$result OR !$result_cal){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					$cart = array(
					"msg"=>"NO DATA!!",				
					);
					$output = json_encode($cart);
					echo($output);
				}else{
					$mArrayWeight=array();	
					$DBData_cal=mysql_fetch_row($result_cal);
					$mUserCal=$DBData_cal[0];
					$mUserWeightFirst=$DBData_cal[1];
					$mUserHeight=$DBData_cal[2];		
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mBMI=$DBData[0]/pow(($mUserHeight/100),2);
						$mArrayWeightData=array(				
						'weight_day' => $DBData[0],
						'waistline' => $DBData[1],
						'body_fat' => number_format($DBData[2],2),
						'BMI' => number_format($mBMI,2),
						'weight_day_date' => $DBData[3]);
						$mArrayWeight+=array($i => $mArrayWeightData);
						$mLastHeight=$DBData[4];				
					}						
						$cart = array(
							'user_data'=>$mArrayWeight,							
							'user_weight_first'=>$mUserWeightFirst,
							'user_height_last'=>$mLastHeight
							);			
						$output = json_encode($cart);
						echo ($output."\n");
				}
				
			}			
		
		}

		//清空每日體重
		public function DB_weight_day_delete($mSessionToken){
			$mToday = date("dmY", strtotime('Saturday this week'));
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "DELETE FROM weight_day WHERE user_id='$mUserID' AND weight_day_date='$mToday'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}							
		}

		//清空每日體重-all
		public function DB_weight_day_delete_all($mSessionToken){			
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "DELETE FROM weight_day WHERE user_id='$mUserID'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}							
		}


		//寫入每日體重
		public function DB_weight_day($mSessionToken,$mWeightDay,$mWaistline,$mHeight){
			$mToday = date("dmY", strtotime('Saturday this week'));
			$mUserID = DB::DB_find_user_id($mSessionToken);
			//計算體脂肪
			$mUserSex = DB::DB_find_user_sex($mSessionToken);
			if($mUserSex==1){
				$mMethodA=$mWaistline*0.74;
				$mMethodB=($mWeightDay*0.082)+34.89;
				$mBodyFatWeight=$mMethodA-$mMethodB;
				$mBodyFat=number_format(($mBodyFatWeight/$mWeightDay)*100,2);
			}elseif($mUserSex==0){
				$mMethodA=$mWaistline*0.74;
				$mMethodB=($mWeightDay*0.082)+44.74;
				$mBodyFatWeight=$mMethodA-$mMethodB;
				$mBodyFat=number_format(($mBodyFatWeight/$mWeightDay)*100,2);
			}else{
				die("ERROR");
			}

			//計算所需卡路里
			$mUserWI = DB::DB_find_user_work_intensity($mSessionToken);
			$mUserAge = DB::DB_find_user_age($mSessionToken);
			if($mUserWI==0){
				$mUserWIM = 1.2;
			}elseif($mUserWI==1){
				$mUserWIM = 1.55;
			}elseif($mUserWI==2) {
				$mUserWIM = 1.9;
			}else{
				die("ERROR");
			}
			if($mUserSex==1){
				$mUserCalNeed=(655+(9.6*$mWeightDay)+(1.8*$mHeight)-(4.7*$mUserAge))*$mUserWIM;
			}elseif($mUserSex==0){
				$mUserCalNeed=(66+(13.7*$mWeightDay)+(5*$mHeight)-(6.8*$mUserAge))*$mUserWIM;
			}else{
				die("ERROR");
			}
			
			$sql_query_calneed = "UPDATE user SET user_cal = '$mUserCalNeed' WHERE user_session_token='$mSessionToken'";
			$result_calneed=mysql_query($sql_query_calneed);

 			$sql_query = "INSERT INTO weight_day(user_id,weight_day,weight_day_date,waistline,body_fat,height)
			VALUE('$mUserID','$mWeightDay','$mToday','$mWaistline','$mBodyFat',$mHeight)";
			$result=mysql_query($sql_query);
			if(!$result OR !$result_calneed){
				die("ERROR");
			}else{
				
				$cart = array(
				"msg"=>"Success"				
				);
				$output = json_encode($cart);
				echo ($output."\n");
			}	
			
		}

		//清空選擇日備忘錄
		public function DB_event_day_delete($mSessionToken,$mEventDay,$mEventDate,$mEventTime){
			
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "DELETE FROM event_day WHERE user_id='$mUserID' AND event_day_date='$mEventDate' 
			AND event_day='$mEventDay' AND event_time='$mEventTime'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				
				$cart = array(
				"msg"=>"Success"				
				);
				$output = json_encode($cart);
				echo ($output."\n");
			}							
		}

		//清空選擇日備忘錄-全部
		public function DB_event_day_delete_all($mSessionToken){
			
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "DELETE FROM event_day WHERE user_id='$mUserID'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}					
		}

		//寫入備忘錄
		public function DB_event_day($mSessionToken,$mEventDay,$mEventDate,$mEventTime){			
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "INSERT INTO event_day(user_id,event_day,event_day_date,event_time)VALUE('$mUserID','$mEventDay','$mEventDate','$mEventTime')";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				
				$cart = array(
				"msg"=>"Success"				
				);
				$output = json_encode($cart);
				echo ($output."\n");
			}	
			
		}
		//取出備忘錄
		public function DB_event_day_show($mSessionToken,$mEventDayDate){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT event_id,event_day,event_time FROM event_day WHERE user_id='$mUserID' AND event_day_date='$mEventDayDate' ORDER BY event_id ASC";
			$result=mysql_query($sql_query);
			
			if(!$result){
				die("ERROR");
			}else{				
				$mArrayEvent=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);										
						$mArrayEventData=array(									
						'event_day' => $DBData[1],	
						'event_time' => $DBData[2]);				
						$mArrayEvent+=array($i => $mArrayEventData);				
					}	
				$cart = array(
				"event"=>$mArrayEvent				
				);				
				$output = json_encode($cart);
				echo ($output."\n");
			}
			
		}

		//選擇食物分區
		public function DB_food_partition($mFoodPartition){
			switch($mFoodPartition){
				case 0:
					$sql_query = "SELECT food_id,food_name,food_cal,food_portion FROM food";
					$result=mysql_query($sql_query);
					
					if(!$result){
						die("ERROR");
					}else{
						$mArrayFood=array();
						for($i=0;$i<=mysql_num_rows($result)-1;$i++)
						{
							$DBData=mysql_fetch_row($result);
							$mArrayFoodData=array(				
							'food_id' => $DBData[0],
							'food_name' => $DBData[1],
							'food_cal' => $DBData[2],
							'food_portion' => $DBData[3]);
							$mArrayFood+=array($i => $mArrayFoodData);				
						}
						
						$cart = array(
							"foodlist"=>$mArrayFood				
							);			
						$output = json_encode($cart);
						echo ($output."\n");
					}
				default:
					$sql_query = "SELECT food_id,food_name,food_cal,food_portion FROM food WHERE food_partition_id LIKE '%,$mFoodPartition,%'";
					$result=mysql_query($sql_query);
					
					if(!$result){
						die("ERROR");
					}else{
						$mArrayFood=array();
						for($i=0;$i<=mysql_num_rows($result)-1;$i++)
						{
							$DBData=mysql_fetch_row($result);
							$mArrayFoodData=array(				
							'food_id' => $DBData[0],
							'food_name' => $DBData[1],
							'food_cal' => $DBData[2],
							'food_portion' => $DBData[3]);
							$mArrayFood+=array($i => $mArrayFoodData);				
						}
						
						$cart = array(
							"foodlist"=>$mArrayFood				
							);			
						$output = json_encode($cart);
						echo ($output."\n");
					}
				}							
		}

		//用名字找食物
		public function DB_food_find_name($mFoodName){
			$sql_query = "SELECT food_id,food_name,food_cal,food_portion FROM food WHERE food_name LIKE '%$mFoodName%'";
			$result=mysql_query($sql_query);
			
				if(!$result){
					die("ERROR");
				}else{
					$mArrayFood=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mArrayFoodData=array(				
						'food_id' => $DBData[0],
						'food_name' => $DBData[1],
						'food_cal' => $DBData[2],
						'food_portion' => $DBData[3]);
						$mArrayFood+=array($i => $mArrayFoodData);				
					}
					
					$cart = array(
						"foodlist"=>$mArrayFood				
						);			
					$output = json_encode($cart);
					echo ($output."\n");
				}
			}
		//給選擇日全部餐飲
		public function DB_food_show_all($mDate,$mSessionToken){
			$mUserCal = DB::DB_find_user_cal($mSessionToken);
			list($mBreakfast,$mBreakfastCal)=DB::DB_food_show_breakfast($mDate,$mSessionToken);
			list($mLunch,$mLunchCal)=DB::DB_food_show_lunch($mDate,$mSessionToken);
			list($mDinner,$mDinnerCal)=DB::DB_food_show_dinner($mDate,$mSessionToken);
			list($mOther,$mOtherCal)=DB::DB_food_show_other($mDate,$mSessionToken);
			$mFoodCalAll=$mBreakfastCal+$mLunchCal+$mDinnerCal+$mOtherCal;
			$cart = array(
					"breakfast_cal"=>$mBreakfastCal,
					"breakfast"=>$mBreakfast,
					"lunch_cal"=>$mLunchCal,
					"lunch"=>$mLunch,
					"dinner_cal"=>$mDinnerCal,
					"dinner"=>$mDinner,
					"other_cal"=>$mOtherCal,
					"other"=>$mOther,
					"foodcal_all"=>$mFoodCalAll,
					"user_cal"=>$mUserCal				
					);			
			$output = json_encode($cart);
			echo ($output."\n");

		}

		//給選擇日早餐
		public function DB_food_show_breakfast($mDate,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT breakfast_food_id,breakfast_food_portion FROM breakfast WHERE user_id='$mUserID'
			 AND breakfast_date='$mDate'";
			$result=mysql_query($sql_query);	
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					return 0;
				}else
				{	
					$mFoodCal=0;			
					$mArrayFood=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mFoodID=$DBData[0];
						$mArrayFoodIDData=DB::DB_food_find_foodID($mFoodID);					
						$mArrayFoodIDData+=array(									
						'meal_portion' => $DBData[1]);
						$mFoodCal+=($mArrayFoodIDData[food_cal]*$mArrayFoodIDData[meal_portion]);					
						$mArrayFood+=array($i => $mArrayFoodIDData);				
					}
					return array($mArrayFood,$mFoodCal);
				}
			}		
											
		}	
			
		//給選擇日午餐
		public function DB_food_show_lunch($mDate,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT lunch_food_id,lunch_food_portion FROM lunch WHERE user_id='$mUserID'
			 AND lunch_date='$mDate'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					return 0;
				}else
				{	
					$mFoodCal=0;			
					$mArrayFood=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mFoodID=$DBData[0];
						$mArrayFoodIDData=DB::DB_food_find_foodID($mFoodID);					
						$mArrayFoodIDData+=array(									
						'meal_portion' => $DBData[1]);
						$mFoodCal+=($mArrayFoodIDData[food_cal]*$mArrayFoodIDData[meal_portion]);					
						$mArrayFood+=array($i => $mArrayFoodIDData);				
					}
					return array($mArrayFood,$mFoodCal);
				}
			}				
		}

		//給選擇日晚餐
		public function DB_food_show_dinner($mDate,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT dinner_food_id,dinner_food_portion FROM dinner WHERE user_id='$mUserID'
			 AND dinner_date='$mDate'";
			$result=mysql_query($sql_query);			
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					return 0;
				}else
				{	
					$mFoodCal=0;			
					$mArrayFood=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mFoodID=$DBData[0];
						$mArrayFoodIDData=DB::DB_food_find_foodID($mFoodID);					
						$mArrayFoodIDData+=array(									
						'meal_portion' => $DBData[1]);
						$mFoodCal+=($mArrayFoodIDData[food_cal]*$mArrayFoodIDData[meal_portion]);					
						$mArrayFood+=array($i => $mArrayFoodIDData);				
					}
					return array($mArrayFood,$mFoodCal);
				}
			}				
		}

		//給選擇日其他
		public function DB_food_show_other($mDate,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT other_food_id,other_food_portion FROM other WHERE user_id='$mUserID'
			 AND other_date='$mDate'";
			$result=mysql_query($sql_query);			
			if(!$result){
				die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					return 0;
				}else
				{	
					$mFoodCal=0;			
					$mArrayFood=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mFoodID=$DBData[0];
						$mArrayFoodIDData=DB::DB_food_find_foodID($mFoodID);					
						$mArrayFoodIDData+=array(									
						'meal_portion' => $DBData[1]);
						$mFoodCal+=($mArrayFoodIDData[food_cal]*$mArrayFoodIDData[meal_portion]);					
						$mArrayFood+=array($i => $mArrayFoodIDData);				
					}
					return array($mArrayFood,$mFoodCal);
				}
			}				
		}

		//以食物ID找食物
		public function DB_food_find_foodID($mFoodID){
			$sql_query = "SELECT food_id,food_name,food_cal,food_portion FROM food WHERE food_id='$mFoodID'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				$DBData=mysql_fetch_row($result);					
				$mArrayFoodData=array(
				'food_id' => $DBData[0],				
				'food_name' => $DBData[1],
				'food_cal' => $DBData[2],
				'food_portion' => $DBData[3]);
				return $mArrayFoodData;
			}			
		}

		//輸入餐點
		public function DB_food_set($mWhatMeal,$mFoodData,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			switch($mWhatMeal){
				case "0":
					$mToday = date("dmY", strtotime('today'));
					for($i=0;$i<count($mFoodData);$i++){
						$mFoodDataID=$mFoodData[$i]->food_id;						
						$mFoodDataPortion=$mFoodData[$i]->breakfast_food_portion;
						$sql_query = "INSERT INTO breakfast(user_id,breakfast_date,breakfast_food_id,breakfast_food_portion
							)VALUE('$mUserID','$mToday','$mFoodDataID','$mFoodDataPortion')";
						$result=mysql_query($sql_query);
						if(!$result){
							die("ERROR");
						}												
					}
					$cart = array(
						"msg"=>"Success"									
					);
					$output = json_encode($cart);
					echo ($output."\n");	
					break;
				case "1":
					$mToday = date("dmY", strtotime('today'));
						for($i=0;$i<count($mFoodData);$i++){
							$mFoodDataID=$mFoodData[$i]->food_id;						
							$mFoodDataPortion=$mFoodData[$i]->lunch_food_portion;
							$sql_query = "INSERT INTO lunch(user_id,lunch_date,lunch_food_id,lunch_food_portion
								)VALUE('$mUserID','$mToday','$mFoodDataID','$mFoodDataPortion')";
							$result=mysql_query($sql_query);
							if(!$result){
								die("ERROR");
							}												
						}
						$cart = array(
							"msg"=>"Success"				
						);
						$output = json_encode($cart);
						echo ($output."\n");						
					break;
				case "2":
					$mToday = date("dmY", strtotime('today'));
						for($i=0;$i<count($mFoodData);$i++){
							$mFoodDataID=$mFoodData[$i]->food_id;						
							$mFoodDataPortion=$mFoodData[$i]->dinner_food_portion;
							$sql_query = "INSERT INTO dinner(user_id,dinner_date,dinner_food_id,dinner_food_portion
								)VALUE('$mUserID','$mToday','$mFoodDataID','$mFoodDataPortion')";
							$result=mysql_query($sql_query);
							if(!$result){
								die("ERROR");
							}												
						}
						$cart = array(
							"msg"=>"Success"				
						);
						$output = json_encode($cart);
						echo ($output."\n");	
					break;
				case "3":
					$mToday = date("dmY", strtotime('today'));
						for($i=0;$i<count($mFoodData);$i++){
							$mFoodDataID=$mFoodData[$i]->food_id;						
							$mFoodDataPortion=$mFoodData[$i]->other_food_portion;
							$sql_query = "INSERT INTO other(user_id,other_date,other_food_id,other_food_portion
								)VALUE('$mUserID','$mToday','$mFoodDataID','$mFoodDataPortion')";
							$result=mysql_query($sql_query);
							if(!$result){
								die("ERROR");
							}												
						}
						$cart = array(
							"msg"=>"Success"				
						);
						$output = json_encode($cart);
						echo ($output."\n");	
					break;
				default:
				echo "Warning wrong number!";
			}			
		}

		//清空餐飲
		public function DB_food_delete($mWhatMeal,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			switch($mWhatMeal){
				case "0":
					$mToday = date("dmY", strtotime('today'));
					$sql_query = "DELETE FROM breakfast WHERE user_id='$mUserID' AND breakfast_date='$mToday'";
						$result=mysql_query($sql_query);
						if(!$result){
							die("ERROR");
						}										
					break;
				case "1":
					$mToday = date("dmY", strtotime('today'));
					$sql_query = "DELETE FROM lunch WHERE user_id='$mUserID' AND lunch_date='$mToday'";
						$result=mysql_query($sql_query);
						if(!$result){
							die("ERROR");
						}								
					break;
				case "2":
					$mToday = date("dmY", strtotime('today'));
					$sql_query = "DELETE FROM dinner WHERE user_id='$mUserID' AND dinner_date='$mToday'";
						$result=mysql_query($sql_query);
						if(!$result){
							die("ERROR");
						}			
					break;
				case "3":
					$mToday = date("dmY", strtotime('today'));
					$sql_query = "DELETE FROM other WHERE user_id='$mUserID' AND other_date='$mToday'";
						$result=mysql_query($sql_query);
						if(!$result){
							die("ERROR");
						}			
					break;
				case "4":
					$sql_query = "DELETE FROM breakfast WHERE user_id='$mUserID'";
					$result=mysql_query($sql_query);
					if(!$result){
						die("ERROR");
					}
					$sql_query = "DELETE FROM lunch WHERE user_id='$mUserID' AND lunch_date='$mToday'";
					$result=mysql_query($sql_query);
					if(!$result){
						die("ERROR");
					}
					$sql_query = "DELETE FROM dinner WHERE user_id='$mUserID' AND dinner_date='$mToday'";
					$result=mysql_query($sql_query);
					if(!$result){
						die("ERROR");
					}
					$sql_query = "DELETE FROM other WHERE user_id='$mUserID' AND other_date='$mToday'";
					$result=mysql_query($sql_query);
					if(!$result){
						die("ERROR");
					}
					break;			
				default:
				echo "Warning wrong number!";
			}			
		}

		//運動清單
		public function DB_sportlist(){
			$sql_query = "SELECT sports_id,sports_name,sports_cal,sports_consume FROM sports";
			$result=mysql_query($sql_query);
			
			if(!$result){
				die("ERROR");
			}else{
				$mArraySport=array();
				for($i=0;$i<=mysql_num_rows($result)-1;$i++)
				{
					$DBData=mysql_fetch_row($result);
					$mArraySportData=array(				
					'sport_id' => $DBData[0],
					'sport_name' => $DBData[1],
					'sport_cal' => $DBData[2],
					'sport_portion' => $DBData[3]);					
					$mArraySport+=array($i => $mArraySportData);				
				}
				
				$cart = array(
					"sportlist"=>$mArraySport				
					);			
				$output = json_encode($cart);
				echo ($output."\n");
			}
											
		}

		//清空運動
		public function DB_sport_delete($mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$mToday = date("dmY", strtotime('today'));
			$sql_query = "DELETE FROM user_sport WHERE user_id='$mUserID' AND sport_date='$mToday'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}										
			
		}

		//清空運動-all
		public function DB_sport_delete_all($mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$mToday = date("dmY", strtotime('today'));
			$sql_query = "DELETE FROM user_sport WHERE user_id='$mUserID'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}										
			
		}

		//以運動ID找運動
		public function DB_sport_find_sport_id($mSportsID){
			$sql_query = "SELECT sports_id,sports_name,sports_cal,sports_consume FROM sports WHERE sports_id='$mSportsID'";
			$result=mysql_query($sql_query);
			if(!$result){
				die("ERROR");
			}else{
				$DBData=mysql_fetch_row($result);					
				$mArraySportData=array(	
				'sports_id' => $DBData[0],			
				'sports_name' => $DBData[1],
				'sports_cal' => $DBData[2],
				'sports_consume' => $DBData[3]);
				return $mArraySportData;
			}			
		}
		//輸入運動
		public function DB_sport_set($mSportData,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$mToday = date("dmY", strtotime('today'));
			for($i=0;$i<count($mSportData);$i++){
				$mSportDataID=$mSportData[$i]->sports_id;						
				$mSportDataCycle=$mSportData[$i]->user_sport_cycle;
				$sql_query = "INSERT INTO user_sport(user_id,sport_date,sports_id,user_sport_cycle
					)VALUE('$mUserID','$mToday','$mSportDataID','$mSportDataCycle')";
				$result=mysql_query($sql_query);
				if(!$result){
					die("ERROR");
				}
			}													
			$cart = array(
				"msg"=>"Success"				
			);
			$output = json_encode($cart);
			echo ($output."\n");	
		}

		//給選擇日運動
		public function DB_sport_show($mDate,$mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT sports_id,user_sport_cycle FROM user_sport WHERE user_id='$mUserID'
			 AND sport_date='$mDate'";
			$result=mysql_query($sql_query);			
			if(!$result){				
				 die("ERROR");
			}else{
				if(mysql_num_rows($result)<1)
				{
					 $cart = array(
					"msg"=>"NO DATA"				
					);
					$output = json_encode($cart);
					echo ($output."\n");
				}else
				{				
					$mArraySport=array();
					for($i=0;$i<=mysql_num_rows($result)-1;$i++)
					{
						$DBData=mysql_fetch_row($result);
						$mSportID=$DBData[0];
						$mArraySportIDData=DB::DB_sport_find_sport_id($mSportID);					
						$mArraySportIDData+=array(									
						'sport_cycle' => $DBData[1]);					
						$mArraySport+=array($i => $mArraySportIDData);				
					}
					$cart = array(
						"sport"=>$mArraySport						
					);			
					$output = json_encode($cart);
					echo ($output."\n");					
				}
			}				
		}

		//幾天前運動
		public function DB_last_sport($mSessionToken){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT sport_date  FROM user_sport WHERE user_id='$mUserID' ORDER BY user_sport_id DESC Limit 1";
			$result=mysql_query($sql_query);			
			if(!$result){				
				 die("ERROR");
			}
			if(mysql_num_rows($result)<1){
				$sql_user_sign_date = "SELECT user_sign_date  FROM user WHERE user_id='$mUserID'";
				$result_user_sign_date=mysql_query($sql_user_sign_date);
				if(!$result_user_sign_date){				
				  die("ERROR");
				}else{
					$DBData=mysql_fetch_row($result_user_sign_date);
					$mLastSportDate=$DBData[0];
					if(strlen($mLastSportDate)==8){
						$mDate=substr_replace($mLastSportDate,'-',2,0);	
						$mDate=substr_replace($mDate,'-',5,0);
					}else{
						$mDate=substr_replace($mLastSportDate,'-',1,0);	
						$mDate=substr_replace($mDate,'-',4,0);
					}
					$mLastSport = (strtotime('today')-strtotime($mDate))/86400;
					$cart = array(
						"msg"=>$mLastSport				
					);
					$output = json_encode($cart);
					echo ($output."\n");		
				}
			}else{
				$DBData=mysql_fetch_row($result);
				$mLastSportDate=$DBData[0];
				if(strlen($mLastSportDate)==8){
					$mDate=substr_replace($mLastSportDate,'-',2,0);	
					$mDate=substr_replace($mDate,'-',5,0);
				}else{
					$mDate=substr_replace($mLastSportDate,'-',1,0);	
					$mDate=substr_replace($mDate,'-',4,0);
				}			
				$mLastSport = (strtotime('today')-strtotime($mDate))/86400;
				$cart = array(
					"msg"=>$mLastSport				
				);
				$output = json_encode($cart);
				echo ($output."\n");
			}			
		}

		//取每日任務
		public function DB_task_show($mSessionToken,$mTaskDate){
			$mUserID = DB::DB_find_user_id($mSessionToken);
			$sql_query = "SELECT * FROM task WHERE task_date='$mTaskDate'";
			$result=mysql_query($sql_query);
			if(!$result){				
				 die("ERROR");
			}
			$mTask=mysql_num_rows($result);
			if($mTask<1){
				$sql_query_sign = "INSERT INTO task(user_id,task_date)VALUE('$mUserID','$mTaskDate')";
				$result_sign=mysql_query($sql_query_sign);
				if(!$result_sign){
					die("ERROR");
				}		
			}
			

			$sql_query_show = "SELECT task_one,task_two,task_three,task_four,task_five FROM task WHERE task_date='$mTaskDate' AND user_id='$mUserID'";
			$result_show=mysql_query($sql_query_show);
			if(!$result_show){
					die("ERROR");
			}else{
				$mArrayTask=array();	
				$DBData=mysql_fetch_row($result_show);
				for($i=0;$i<5;$i++)
				{					
					$mArrayTaskData=array(
					'data' => $DBData[$i]);
					$mArrayTask+=array($i => $mArrayTaskData);	
				}						
				$cart = array(
					'task'=>$mArrayTask						
				);			
				$output = json_encode($cart);
				echo ($output."\n");
			}	
		}

		//完成每日任務
		public function DB_task_set($mSessionToken,$mTaskDate,$mTask){
			$mUserID = DB::DB_find_user_id($mSessionToken);	
			switch ($mTask) {
				case '0':
					$mTaskData="task_one";
					break;
				case '1':
					$mTaskData="task_two";
					break;
				case '2':
					$mTaskData="task_three";
					break;
				case '3':
					$mTaskData="task_four";
					break;			
				case '4':
					$mTaskData="task_five";
					break;				
				default:
					die("Warning wrong number!");
					break;
			}	
			//任務狀態
			$sql_query_task = "SELECT $mTaskData FROM task WHERE user_id='$mUserID' AND task_date='$mTaskDate'";
			$result_task=mysql_query($sql_query_task);
			if(!$result_task){
				die("ERROR");
			}else{
				$DBData_task=mysql_fetch_row($result_task);
				$mTaskNow=$DBData_task[0];
				if($mTaskNow==0){
					//任務完成
					$sql_query = "UPDATE task SET $mTaskData = 1 WHERE user_id='$mUserID' AND task_date='$mTaskDate'";
					$result=mysql_query($sql_query);
					if(!$result){
						die("ERROR");
					}
					//增加成就點數
					$sql_query_pointshow = "SELECT user_task_point from user WHERE user_id='$mUserID'";
					$result_pointshow=mysql_query($sql_query_pointshow);
					if(!$result_pointshow){
						die("ERROR");
					}else{
						$DBData=mysql_fetch_row($result_pointshow);
						$mUserPoint=$DBData[0]+10;
					}
					//更新點數
					$sql_query_point = "UPDATE user SET user_task_point = '$mUserPoint' WHERE user_id='$mUserID'";
					$result_point=mysql_query($sql_query_point);
					if(!$result_point){
						die("ERROR");
					}

					$cart = array(
						"msg"=>"Success"				
					);
					$output = json_encode($cart);
					echo ($output."\n");	
				}else{
					$cart = array(
						"msg"=>"Task is complete"				
					);
					$output = json_encode($cart);
					echo ($output."\n");	
				}
			}			
		}

		

		//解構子
		function __destruct(){
						
		}
}

?>